import firebase from 'firebase';
import firestore from 'firebase/firestore'

var firebaseConfig = {
    apiKey: "AIzaSyDhp7h_ia3L1ucB1sPGhMJHLo3VKb2OH-8",
    authDomain: "udemey-real-chat.firebaseapp.com",
    databaseURL: "https://udemey-real-chat.firebaseio.com",
    projectId: "udemey-real-chat",
    storageBucket: "udemey-real-chat.appspot.com",
    messagingSenderId: "1015544361758",
    appId: "1:1015544361758:web:ff938a96166681f61c4f0f"
  };
  // Initialize Firebase
  const firebaseApp = firebase.initializeApp(firebaseConfig);
  firebaseApp.firestore().settings({ timestampsInSnapshots: true })

  export default firebaseApp.firestore();